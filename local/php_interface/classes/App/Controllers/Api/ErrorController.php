<?php
/**
 *
 */

declare(strict_types=1);

namespace App\Controllers\Api;

use App\Api\Result;

/**
 * Контроллер ошибок API
 *
 * Class ErrorController
 *
 * @package App\Api
 */
class ErrorController extends AbstractApiController
{
    public function error404(): void
    {
        $result = new Result(null, 404);
        $result->addError('Метод не найден');

        static::renderResult($result);
    }

    public function error500(): void
    {
        $result = new Result(null, 500);
        $result->addError('Ошибка при обработке запроса');

        static::renderResult($result);
    }

    public function errorDemo(): void
    {
        $result = new Result(null, 200);
        $result->addError('Проверка работы API');

        static::renderResult($result);
    }
}