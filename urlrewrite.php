<?php
/**
 *
 */
declare(strict_types=1);
$arUrlRewrite = [
    [
        'CONDITION' => '#^\\/?\\/mobileapp/jn\\/(.*)\\/.*#',
        'RULE'      => 'componentName=$1',
        'ID'        => null,
        'PATH'      => '/bitrix/services/mobileapp/jn.php',
        'SORT'      => 100,
    ],
    [
        'CONDITION' => '#^/rest/#',
        'RULE'      => '',
        'ID'        => null,
        'PATH'      => '/bitrix/services/rest/index.php',
        'SORT'      => 100,
    ],
    [
        'CONDITION' => '#^/api/v1/iblock/(\d*)#',
        'RULE'      => 'id=$1',
        'ID'        => '',
        'PATH'      => '/api/v1/handler.php'
    ],
    [
        'CONDITION' => '#^/api/v1/highload/(\d*)#',
        'RULE'      => 'id=$1',
        'ID'        => '',
        'PATH'      => '/api/v1/handler.php'
    ],
    [
        'CONDITION' => '#^/api/v1/#',
        'RULE'      => '',
        'ID'        => '',
        'PATH'      => '/api/v1/handler.php'
    ],
];
