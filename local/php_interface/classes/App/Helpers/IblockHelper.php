<?php
/**
 * Created by PhpStorm.
 * User: andrey
 * Date: 01.09.2020
 * Time: 0:59
 */
declare(strict_types=1);

namespace App\Helpers;

use Bitrix\Iblock\IblockTable;
use Bitrix\Main\Loader;

/**
 * Class IblockHelper
 *
 * @package App\Helpers
 */
class IblockHelper
{
    /**
     * @param string $type
     * @param string $code
     *
     * @return int|null
     * @throws \Bitrix\Main\ArgumentException
     * @throws \Bitrix\Main\LoaderException
     * @throws \Bitrix\Main\ObjectPropertyException
     * @throws \Bitrix\Main\SystemException
     */
    public static function getIblockId(string $type, string $code): ?int
    {
        $res = null;

        if (Loader::includeModule('iblock')) {
            $resIblockId = IblockTable::getList(
                [
                    'select' => [
                        'ID'
                    ],
                    'filter' => [
                        'CODE'           => $code,
                        'IBLOCK_TYPE_ID' => $type
                    ]
                ]
            );

            $row = (array)$resIblockId->fetch();
            $res = (int)$row['ID'];
        }

        return $res;
    }
}