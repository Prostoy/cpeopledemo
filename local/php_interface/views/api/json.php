<?php
/**
 *
 */
declare(strict_types=1);

use App\Api\Result;
use Bitrix\Main\Engine\Response\Json as JsonResponse;

/** @var array $data */

/** @var \App\Api\Result $result */
$result = $data['result'];

if (!($result instanceof Result)) {
    //todo: Записать в лог
    $result = new Result([], 500);
}

$response = new JsonResponse($result->toArray(), JSON_FORCE_OBJECT);
$response->setStatus($result->getStatus());
$response->send();
