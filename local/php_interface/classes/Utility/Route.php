<?php
/**
 * Created by PhpStorm.
 * User: andrey
 * Date: 30.08.2020
 * Time: 20:38
 */
declare(strict_types=1);

namespace Utility;

/**
 * Class Route
 *
 * @package Utility
 */
class Route
{
    /**
     * @var string
     */
    protected $name;

    /**
     * @var string
     */
    protected $path;

    /**
     * @var array
     */
    protected $controller;

    /**
     * @var string
     */
    protected $method;

    /**
     * Route constructor.
     *
     * @param string $name
     * @param string $path
     */
    public function __construct(
        string $name,
        string $path
    ) {
        $this->name = $name;
        $this->path = $path;
    }

    /**
     * @param callable $controller
     *
     * @return \Utility\Route
     */
    public function controller(callable $controller): Route
    {
        $this->controller = $controller;
        return $this;
    }

    /**
     * @param string $method
     *
     * @return Route
     */
    public function method(string $method): Route
    {
        $this->method = $method;
        return $this;
    }

    /**
     * @return string|null
     */
    public function getMethod(): ?string
    {
        return $this->method;
    }

    /**
     * @return array|null
     */
    public function getController(): ?array
    {
        return $this->controller;
    }

    /**
     * @return string
     */
    public function getPath(): string
    {
        return $this->path;
    }

    /**
     * @return string
     */
    public function getName(): string
    {
        return $this->name;
    }
}