<?php
/**
 *
 */
declare(strict_types=1);

namespace App\Api;

/**
 * Class Result
 *
 * @package App\Api
 */
class Result
{
    protected $errors;
    protected $data;
    protected $status;

    /**
     * Result constructor.
     *
     * @param int   $status
     * @param array $data
     */
    public function __construct(array $data = null, int $status = 200)
    {
        $this->data = $data;
        $this->status = $status;
        $this->errors = [];
    }

    /**
     * @param string $error
     *
     * @return $this
     */
    public function addError(string $error): self
    {
        $this->errors[] = $error;
        return $this;
    }

    /**
     * @return array
     */
    public function toArray(): array
    {
        return [
            'data' => $this->data,
            'errors' => $this->errors
        ];
    }

    /**
     * @return int
     */
    public function getStatus(): int
    {
        return $this->status;
    }
}