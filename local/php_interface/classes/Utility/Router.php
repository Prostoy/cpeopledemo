<?php
declare(strict_types=1);

namespace Utility;

use Controllers\AbstractController;
use Controllers\RequestControllerInterface;
use LogicException;
use \RuntimeException;
use Bitrix\Main\HttpRequest as Request;
use function str_replace;

/**
 * Class Router
 *
 * @package Utility
 */
class Router
{
    /**
     * @var \Bitrix\Main\HttpRequest
     */
    protected $request;

    /**
     * @var
     */
    protected $routes;

    /**
     * Router constructor.
     *
     * @param \Bitrix\Main\HttpRequest $request
     */
    public function __construct(Request $request)
    {
        $this->request = $request;
    }

    /**
     *
     */
    public function route(): void
    {
        $route = $this->matchRequest();

        if ($route) {
            $controller = $route->getController();
        } else {
            throw new RuntimeException('Маршрут не найден');
        }

        if ($controller) {
            [$controllerClassName, $methodName] = $controller;
        } else {
            throw new RuntimeException(
                'Не установлен контроллер маршрута'
            );
        }

        if ($controllerClassName && class_exists($controllerClassName)) {
            $controller = new $controllerClassName;
        } else {
            throw new RuntimeException(
                'Не найден контроллер ' . $controllerClassName
            );
        }

        if ($controller instanceof AbstractController) {
            $controller->setDefaults();
        }

        if ($methodName && !method_exists($controller, $methodName)) {
            throw new RuntimeException(
                'Не найден метод ' . $controllerClassName . '::' . $methodName
            );
        }

        if ($controller instanceof RequestControllerInterface) {
            $controller->setRequest($this->request);
        }

        $controller->$methodName();
    }

    /**
     * @param array $routes
     *
     * @return $this
     */
    public function setRoutes(array $routes): self
    {
        foreach ($routes as $route) {
            if ($route instanceof Route) {
                $this->routes[$route->getPath()][] = $route;
            } else {
                throw new LogicException(
                    'Объект не является экземпляром класса ' . Route::class
                );
            }
        }

        return $this;
    }

    /**
     * @return \Utility\Route|null
     */
    protected function matchRequest(): ?Route
    {
        $path = $this->request->getRequestedPage();
        $path = str_replace('index.php', '', $path);

        $path2 = $this->request->getRequestedPageDirectory();

        $routes = [];
        if ($this->routes[$path]) {
            $routes = (array)$this->routes[$path];
        } elseif ($this->routes[$path2]) {
            $routes = (array)$this->routes[$path2];
        }

        $resultRoute = null;

        /** @var \Utility\Route $route */
        foreach ($routes as $route) {
            if (!$route->getMethod() || $this->request->getRequestMethod() === $route->getMethod()) {
                $resultRoute = $route;
                break;
            }
        }

        return $resultRoute;
    }
}