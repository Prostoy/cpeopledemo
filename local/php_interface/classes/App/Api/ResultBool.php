<?php
/**
 *
 */
declare(strict_types=1);

namespace App\Api;

/**
 * Class ResultBool
 *
 * @package App\Api
 */
class ResultBool extends Result
{
    /**
     * ResultBool constructor.
     *
     * @param bool $success
     * @param int  $status
     */
    public function __construct(bool $success = null, int $status = 200)
    {
        parent::__construct(['success' => $success], $status);
    }
}