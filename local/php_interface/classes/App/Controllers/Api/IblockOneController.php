<?php
/**
 * Created by PhpStorm.
 * User: andrey
 * Date: 01.09.2020
 * Time: 0:52
 */
declare(strict_types=1);

namespace App\Controllers\Api;

use App\Api\Result;
use App\Helpers\IblockHelper;
use Bitrix\Main\Loader;
use CIBlockElement;
use Exception;
use LogicException;

/**
 * Class IblockOneController
 *
 * @package App\Controllers\Api
 */
class IblockOneController extends AbstractApiController
{
    /**
     * Код типа инфоблоков
     */
    const IBLOCK_TYPE = 'demo';

    /**
     * Код инфоблока
     */
    const IBLOCK_CODE = 'demo';

    /**
     * Выводит json конкретного элемента ИБ
     */
    public function get()
    {
        try {
            $id = (int)$this->request->get('id');
            if (!$id) {
                throw new LogicException('Не указан ID элемента в запросе');
            }

            $res = CIBlockElement::GetList(
                [],
                [
                    'ID'        => $id,
                    'IBLOCK_ID' => IblockHelper::getIblockId(static::IBLOCK_TYPE, static::IBLOCK_CODE)
                ],
                false,
                false,
                [
                    'PROPERTY_TEXT',
                    'PROPERTY_IS_TURNED_ON_VALUE'
                ]
            );

            if (!$res) {
                throw new LogicException('Не найден элемент с ID: ' . $id);
            }
            $element = $res->GetNext();

            if (!$element || 0 > count($element)) {
                throw new LogicException('Не найден элемент с ID: ' . $id);
            }

            $result = new Result(
                [
                    'TEXT'         => $element['PROPERTY_TEXT_VALUE']['TEXT'],
                    'IS_TURNED_ON' => (bool)$element['PROPERTY_IS_TURNED_ON_VALUE_VALUE']
                ]
            );
        } catch (Exception $e) {
            $result = new Result(null);
            $result->addError($e->getMessage());
            //todo: Записать в лог
        }

        static::renderResult($result);
    }

    public function add()
    {
        $params = [
            'TEXT'         => $this->request->get('TEXT'),
            'IS_TURNED_ON' => $this->request->get('IS_TURNED_ON') === 'true',
        ];

        try {
            if (!$params['TEXT']) {
                throw new LogicException('Не указан параметр: ' . 'TEXT');
            }

            $adder = new CIBlockElement();
            $element = [
                'NAME'            => 'Элемент',
                'IBLOCK_ID'       => IblockHelper::getIblockId(static::IBLOCK_TYPE, static::IBLOCK_CODE),
                'PROPERTY_VALUES' => [
                    'TEXT'         => $params['TEXT'],
                    'IS_TURNED_ON' => $params['IS_TURNED_ON'],
                ]
            ];

            $addResult = $adder->Add($element);

            if (!$addResult) {
                throw new LogicException('Ошибка при добавлении элемента: ' . $adder->LAST_ERROR);
            }

            $result = new Result(['id' => (int)$addResult]);
        } catch (Exception $e) {
            $result = new Result(null);
            $result->addError($e->getMessage());
            //todo: Записать в лог
        }

        static::renderResult($result);
    }

    /**
     * @throws \Bitrix\Main\LoaderException
     */
    public function setDefaults(): void
    {
        if (!Loader::includeModule('iblock')) {
            throw new LogicException('Не подключен модуль инфоблоков');
        }
    }
}