<?php
/**
 * Created by PhpStorm.
 * User: andrey
 * Date: 01.09.2020
 * Time: 2:47
 */
declare(strict_types=1);

namespace App\Controllers\Api;

use App\Api\Result;
use Bitrix\Highloadblock\HighloadBlockTable;
use Bitrix\Main\Loader;
use Exception;
use LogicException;

/**
 * Class HighloadTwoController
 *
 * @package App\Controllers\Api
 */
class HighloadTwoController extends AbstractApiController
{
    public const HL_BLOCK_ID = 1;

    /**
     * @var string
     */
    protected $dataClass;

    public function add()
    {
        $params = [
            'UF_TEXT'   => $this->request->get('UF_TEXT'),
            'UF_ACTIVE' => $this->request->get('UF_ACTIVE') === 'true',
        ];

        try {
            if (!$params['UF_TEXT']) {
                throw new LogicException('Не указан параметр: ' . 'UF_TEXT');
            }

            /** @var \Bitrix\Main\ORM\Data\Result $addResult */
            $addResult = $this->dataClass::add([
                'UF_TEXT'   => $params['UF_TEXT'],
                'UF_ACTIVE' => $params['UF_ACTIVE']
            ]);

            if (0 < count($addResult->getErrorMessages())) {
                throw new LogicException('Ошибка при добавлении элемента: ' . $addResult->getErrorMessages()[0]);
            }

            $result = new Result(['id' => (int)$addResult->getId()]);
        } catch (Exception $e) {
            $result = new Result(null);
            $result->addError($e->getMessage());
            //todo: Записать в лог
        }

        static::renderResult($result);
    }


    /**
     * Выводит json конкретного элемента ИБ
     */
    public function get()
    {
        try {
            $id = (int)$this->request->get('id');
            if (!$id) {
                throw new LogicException('Не указан ID элемента в запросе');
            }

            $res = $this->dataClass::getList([
                'select' => ['UF_TEXT', 'UF_ACTIVE'],
                'order'  => [],
                'limit'  => '1',//ограничиваем выборку 10-ю элементами
                'filter' => ['ID' => $id]
            ]);

            if (!$res) {
                throw new LogicException('Не найден элемент с ID: ' . $id);
            }
            $element = $res->fetch();

            if (!$element || 0 > count($element)) {
                throw new LogicException('Не найден элемент с ID: ' . $id);
            }

            $element['UF_ACTIVE'] = (bool)$element['UF_ACTIVE'];

            $result = new Result(
                $element
            );
        } catch (Exception $e) {
            $result = new Result(null);
            $result->addError($e->getMessage());
            //todo: Записать в лог
        }

        static::renderResult($result);
    }

    /**
     * @throws \Bitrix\Main\ArgumentException
     * @throws \Bitrix\Main\LoaderException
     * @throws \Bitrix\Main\ObjectPropertyException
     * @throws \Bitrix\Main\SystemException
     */
    public function setDefaults(): void
    {
        Loader::includeModule('highloadblock');
        $hlblock = HighloadBlockTable::getById(static::HL_BLOCK_ID)->fetch();
        $entity = HighloadBlockTable::compileEntity($hlblock);
        $this->dataClass = $entity->getDataClass();
    }
}