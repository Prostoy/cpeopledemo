<?php
/**
 *
 */
declare(strict_types=1);

namespace Controllers;

use Bitrix\Main\HttpRequest as Request;
use Utility\View;

/**
 * Class AbstractController
 *
 * @package Controllers
 */
abstract class AbstractController implements RequestControllerInterface
{
    /**
     * @var Request
     */
    protected $request;

    /**
     * @return Request
     */
    public function getRequest(): Request
    {
        return $this->request;
    }

    /**
     * @param \Bitrix\Main\HttpRequest $request
     *
     * @return \Controllers\RequestControllerInterface
     */
    public function setRequest(Request $request): RequestControllerInterface
    {
        $this->request = $request;
        return $this;
    }

    /**
     * @param string     $template
     * @param array|null $data
     */
    protected static function render(string $template, array $data = null): void
    {
        View::render(
            $template,
            $data = $data ?: []
        );
    }

    abstract public function setDefaults(): void;
}