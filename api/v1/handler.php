<?php
/**
 *
 */
declare(strict_types=1);

/**
 *
 */
define('STOP_STATISTICS', true);

/**
 *
 */
define('NO_KEEP_STATISTIC', 'Y');

/**
 *
 */
define('NO_AGENT_STATISTIC', 'Y');

/**
 *
 */
define('NO_AGENT_CHECK', true);

/**
 *
 */
define('DisableEventsCheck', true);

/**
 *
 */
define('NEED_AUTH', false);

/**
 *
 */
define('DISABLE_HTTP_AUTH', false);

/**
 *
 */
define('SKIP_CONTENT_CHECK', true);

require_once $_SERVER['DOCUMENT_ROOT'] .
    '/bitrix/modules/main/include/prolog_before.php';

use App\Controllers\Api\ErrorController as ApiErrorController;
use App\Controllers\Api\HighloadTwoController;
use App\Controllers\Api\IblockOneController;
use Bitrix\Main\Application;
use Bitrix\Main\SystemException;
use Utility\Route;
use Utility\Router;

try {
    $request = Application::getInstance()->getContext()->getRequest();
} catch (SystemException $e) {
    //todo: Записать в лог
    throw new RuntimeException('Ошибка ядра Битрикс');
}

$routeList = [
    (new Route('iblock/addElement', '/api/v1/iblock/add'))
        ->method('POST')
        ->controller([IblockOneController::class, 'add']),

    (new Route('iblock/getElement', '/api/v1/iblock'))
        ->method('GET')
        ->controller([IblockOneController::class, 'get']),

    (new Route('highload/addElement', '/api/v1/highload/add'))
        ->method('POST')
        ->controller([HighloadTwoController::class, 'add']),

    (new Route('highload/getElement', '/api/v1/highload'))
        ->method('GET')
        ->controller([HighloadTwoController::class, 'get']),
];

$router = (new Router($request))
    ->setRoutes($routeList);

try {
    $router->route();
} catch (Exception $e) {
    (new ApiErrorController())->error404();

    //todo: Удалить, передавать логгер в роутер и писать в лог
    throw $e;
}



