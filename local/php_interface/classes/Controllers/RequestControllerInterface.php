<?php
/**
 *
 */
declare(strict_types=1);

namespace Controllers;

use Bitrix\Main\HttpRequest as Request;

/**
 * Interface RequestControllerInterface
 *
 * @package App\Controllers\Api
 */
interface RequestControllerInterface
{
    /**
     * @return Request
     */
    public function getRequest(): Request;

    /**
     * @param Request $request
     *
     * @return $this
     */
    public function setRequest(Request $request): self;
}