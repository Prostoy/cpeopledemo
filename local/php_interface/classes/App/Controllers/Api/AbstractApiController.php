<?php
/**
 *
 */
declare(strict_types=1);

namespace App\Controllers\Api;

use App\Api\Result;
use Controllers\AbstractController;

/**
 * Class AbstractApiController
 *
 * @package App\Controllers\Api
 */
abstract class AbstractApiController extends AbstractController
{
    /**
     * @param \App\Api\Result $result
     */
    protected static function renderResult(Result $result): void
    {
        static::render('api/json.php', ['result' => $result]);
    }

    public function setDefaults(): void
    {
        // nothing to set
    }
}