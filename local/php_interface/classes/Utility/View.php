<?php
declare(strict_types=1);

namespace Utility;

/**
 * Class View
 *
 * @package Utility
 */
class View
{
    /**
     * @var string
     */
    protected static $path;

    /**
     * @param string $template
     * @param array  $data
     */
    public static function render(string $template, array $data = []): void
    {
        /**
         * @var array $data
         */
        include static::getPath() . $template;
    }

    /**
     * @return string
     */
    public static function getPath(): string
    {
        static::$path = static::$path ?: dirname(__DIR__, 2) . '/views/';

        return static::$path;
    }

    /**
     * @param string $path
     */
    public static function setPath(string $path): void
    {
        self::$path = $path;
    }
}